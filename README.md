# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) and created using [React-Semantic UI](https://react.semantic-ui.com/)

## Access Project.
Project is accessable via gitlab pages [link](https://ayblaq.gitlab.io/fleet-map)
## About Project and Limitations. 

This project makes an API request to fetch vehicle trajectories based on specific date and id. 
The trajectories are rendered on a map using Google Map Direction's API. There are some limitations with Google Map Directions like 
limits on maximum number of waypoints (25 waypoints per requests). An hack was written to create groups of waypoints and also add delays 
after every 10 requests per second due to another Google map request limitation. 

Based on this added hack, time it takes to render trajectories on the map depends on the number of trajectories. 

In addition, using indexedDb to cache last trajectory request was not completed and this solution could have reduced the rendering time of the map. This will be added in the next version. 

Due to time constraint, the following requirements were not met:
- Render best path on Map and calculate the best path distance. 



### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!



