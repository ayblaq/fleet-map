import React from "react";
import { Container, Menu } from "semantic-ui-react";

const SiteHeader = () => {
  return (
    <Menu fixed="top" inverted>
      <Container>
        <Menu.Item as="a">Home</Menu.Item>
      </Container>
    </Menu>
  );
};

export default SiteHeader;
