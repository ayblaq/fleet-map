import React from 'react';
import {Container, Grid, Header, Segment, Icon } from 'semantic-ui-react';

const Footer = () => {
    return (
        <Segment inverted vertical style={{margin: '2em 0em 0em', padding: '2em 0em'}}>
            <Container>
                <p>Developed with love. <Icon color='red' name="like"/></p>
                <Grid>
                    <Grid.Column width={12}>
                        <Header/>
                    </Grid.Column>
                </Grid>
            </Container>
        </Segment>
    )
}

export default Footer;