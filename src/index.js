import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import reportWebVitals from './reportWebVitals';
import { Root } from "./app/components";
import { Routing, history } from "./routing";
import { store } from "./store";
import "./styles/index.scss";
import "semantic-ui-css/semantic.min.css";

if (process.env.NODE_ENV === "development") {
  console.log("This is development");
}

render(
  <Provider store={store}>
    <Root
      routes={Routing}
      history={history}
      store={store}
      rKey={Math.random()}
    />
  </Provider>,
  document.getElementById("root")
);

reportWebVitals();
