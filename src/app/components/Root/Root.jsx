import React from "react";
import { Provider, ReactReduxContext } from "react-redux";
import { ConnectedRouter as Router } from "connected-react-router";

const Root = (props) => {
  const { store, history, routes, rKey } = props;
  
  return (
    <Provider store={store} key={rKey} context={ReactReduxContext}>
      <Router history={history} key={rKey} context={ReactReduxContext}>
        {routes()}
      </Router>
    </Provider>
  );
};

export default Root;
