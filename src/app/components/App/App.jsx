import React from "react";
import { withRouter } from "react-router";
import { Container } from "semantic-ui-react";
import { DBConfig } from '../../../config/db/DBConfig';
import { initDB } from 'react-indexed-db';
import "../../../../src/app/components/App/App.scss";

initDB(DBConfig);

const App = (props) => {
  let { children } = props;
  
  return (
    <div className="page-layout">
      <main>
        {children}
        <div className="main-content">
          <Container></Container>
        </div>
      </main>
    </div>
  );
};

export default withRouter(App);
