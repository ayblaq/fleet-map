import {axiosClient} from '../../api';

export async function fetchVehiclesApi(api_key) {
    return await axiosClient.get(`/getLastData?key=${api_key}&json`)
        .then((response) => {
            return response.data;
        })
        .catch(error => {
            throw error
        })
}

export async function fetchVehicleDataApi(api_key, vehicle_id, start_date, end_date) {
    return await axiosClient.get(`/getRawData?key=${api_key}&objectId=${vehicle_id}&begTimestamp=${start_date}&endTimestamp=${end_date}&json`)
        .then((response) => {
            return response.data;
        })
        .catch(error => {
            throw error
        })
}
