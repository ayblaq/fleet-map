import { fetchVehiclesApi } from '../api/api';
import moment from 'moment';
export const FETCH_VEHICLES_REQUEST = 'FETCH_VEHICLES_REQUEST';
export const FETCH_VEHICLES_SUCCESS = 'FETCH_VEHICLES_SUCCESS';
export const FETCH_VEHICLES_FAILURE = 'FETCH_VEHICLES_FAILURE';

export const fetchVehiclesRequest = () => ({
    type: FETCH_VEHICLES_REQUEST,
    payload: {
        vehicles: []
    }
})

export const fetchVehiclesSuccess = (vehicles) => ({
    type: FETCH_VEHICLES_SUCCESS,
    payload: {
        vehicles: vehicles
    }
})

export const fetchVehiclesFailure = (message) => ({
    type: FETCH_VEHICLES_FAILURE,
    payload: new Error(message)
})

export const fetchVehicles = (api_key) => (dispatch) => {
    dispatch(fetchVehiclesRequest())
    fetchVehiclesApi(api_key)
    .then (data => {
        dispatch(fetchVehiclesSuccess(data.response))
    })
    .catch (error => {
        let message = error.response !== undefined ? error.response.data.errormessage : "Technical error occurred. Contact developer"
        dispatch(fetchVehiclesFailure(message))
    })
}

export const cleanData = (data) => {
    if (data.timestamp) {
        data.time_ago = moment(data.timestamp).fromNow()
    }
    data.speed = data.speed ? data.speed : 0;

    return data;
}