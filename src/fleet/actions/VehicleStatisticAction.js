import { fetchVehicleDataApi } from "../api/api";
import { FetchVehicleHistory, AddVehicleHistory } from "../query/query";
import moment from "moment";
export const FETCH_VEHICLE_STATS_REQUEST = "FETCH_VEHICLE_STATS_REQUEST";
export const FETCH_VEHICLE_STATS_SUCCESS = "FETCH_VEHICLE_STATS_SUCCESS";
export const FETCH_VEHICLE_STATS_FAILURE = "FETCH_VEHICLE_STATS_FAILURE";
export const SET_VEHICLE_ID = "SET_VEHICLE_ID";

export const fetchVehicleStatRequest = (start_date) => ({
  type: FETCH_VEHICLE_STATS_REQUEST,
  payload: {
    start_date: start_date,
  },
});

export const fetchVehicleStatSuccess = (
  stats,
  vehicle_path = [],
  optimised_path = []
) => ({
  type: FETCH_VEHICLE_STATS_SUCCESS,
  payload: {
    data: stats,
    vehicle_path: vehicle_path,
    optimised_path: optimised_path,
  },
});

export const fetchVehicleStatFailure = (message) => ({
  type: FETCH_VEHICLE_STATS_FAILURE,
  payload: new Error(message),
});

export const setVehicleStatId = (objectId, maxDate) => ({
  type: SET_VEHICLE_ID,
  payload: {
    vehicle_id: objectId,
    max_date: maxDate,
  },
});

var delayFactor = 0;
var object_id = null;
var id = null;
var query_date = null;
let aggregated_path = [];
let aggregated_best_path = [];
let newPaths = [];
let paths = [];
const getRoutes = (DirectionsService, stop, index, records, dispatch) => {
  let origin = stop.shift().location;
  let destination = stop.pop().location;

  DirectionsService.route(
    {
      origin: origin,
      destination: destination,
      travelMode: window.google.maps.TravelMode.DRIVING,
      waypoints: stop
    },
    (result, status) => {
      if (status === window.google.maps.DirectionsStatus.OK) {
        aggregated_path.push(result);
        //for IndexedDB integration.
        //AddVehicleHistory(id, object_id, query_date, result, moment(paths[paths.length-1].timestamp).format("YYYY-MM-DD HH:MM:SS"))
        if (records.length === index + 1) {
          dispatch(fetchVehicleStatSuccess(paths, aggregated_path));
          //buggy when data is huge.
          // fetchFastestPath(dispatch, DirectionsService, records)
        }
      } else if (
        status === window.google.maps.DirectionsStatus.OVER_QUERY_LIMIT
      ) {
        delayFactor++;
        setTimeout(function () {
          getRoutes(DirectionsService, stop, index, records, dispatch);
        }, delayFactor * 1000);
      } else {
        dispatch(fetchVehicleStatSuccess(paths, []))
      }
    }
  );
};

const fetchPath = (dispatch) => {
  const DirectionsService = new window.google.maps.DirectionsService();
  let stops = [];
  let count = 0;
  let records = [];

  //Vehicle updates arrive every minute but to render on map, we step by 3.
  for (let i = 0; i < paths.length - 3; i = i + 3) {
    let item = paths[i];
    if (
      item.Latitude === null ||
      item.Longitude === null ||
      item.EngineStatus === null ||
      item.Speed === null
    )
      continue;
    if (
      paths[i + 3]["Latitude"] !== item.Latitude &&
      paths[i + 3]["Longitude"] !== item.Longitude
    ) {
      newPaths.push(paths[i]);
      count = count + 1;
      stops.push({
        location: {
          lat: paths[i]["Latitude"],
          lng: paths[i]["Longitude"],
        },
        stopover: false,
      });
      //Google maps hack for waypoints limit. Google map only allows 25 waypoints per request.
      if (count % 25 === 0) {
        records.push(stops);
        stops = [];
      }
    }
  }

  if (newPaths.length <= 1) {
    dispatch(fetchVehicleStatSuccess(paths, []));
  } else {
    if (records.length < 2 || stops.length > 0) {
      stops.push({
        location: {
          lat: newPaths[newPaths.length - 1]["Latitude"],
          lng: newPaths[newPaths.length - 1]["Longitude"],
        },
        stopover: false,
      });
      records.push(stops);
    }
  
    for (let i = 0; i < records.length; i++) {
      if (i % 9 === 0 && i !== 0) sleep(1000);
      let stop = records[i];
      //google maps limit hack for direction data. Google only allows 10 requests per second. 
      getRoutes(DirectionsService, stop, i, records, dispatch);
    }
  }
};

const sleep = (milliseconds) => {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
};


const getAlternativeRoutes = (DirectionsService, stop, index, records, dispatch) => {
  let origin = stop.shift().location;
  let destination = stop.pop().location;
  DirectionsService.route(
    {
      origin: origin,
      destination: destination,
      travelMode: window.google.maps.TravelMode.DRIVING,
      waypoints: stop,
      provideRouteAlternatives: true,
    },
    (result, status) => {
      if (status === window.google.maps.DirectionsStatus.OK) {
        result = computeFastestRoute(result)
        aggregated_best_path.push(result);
        if (records.length === index + 1) {
          dispatch(fetchVehicleStatSuccess(paths, aggregated_path, aggregated_best_path));
        }
      } else if (
        status === window.google.maps.DirectionsStatus.OVER_QUERY_LIMIT
      ) {
        delayFactor++
        setTimeout(() => {
          getAlternativeRoutes(DirectionsService, stop, index, records, dispatch)
        }, delayFactor * 1000);
      } else {
        dispatch(fetchVehicleStatSuccess(paths, aggregated_path));
      }
    }
  );
}

const fetchFastestPath = (
  dispatch,
  DirectionsService,
  records,
) => {
  for (let i = 0; i < records.length; i++) {
    if (i % 9 === 0 && i !== 0) sleep(1000);
    let stop = records[i];
    //google maps limit hack for direction data. Google only allows 10 requests per second. 
    getAlternativeRoutes(DirectionsService, stop, i, records, dispatch);
  }
};

export const computeFastestRoute = (response) => {
  let shortest = Math.max();
  let mainroute = response.routes[0];
  for (var i = 0; i < response.routes.length; i++) {
    let route = response.routes[i];
    let distance = 0;
    route.legs.map((value, index) => {
      return (distance += value.distance.value);
    }, distance);

    if (distance < shortest) {
      shortest = distance;
      mainroute = response.routes[i];
    }
  }
  response.routes = [mainroute];

  return response;
};

//splice vehicle data based on last query
const filterData = (path, history) => {
  let split_index = 0;
  for (let i = 0; i < path.length; i++) {
    let path_date = moment(path[i].timestamp).format("YYYY-MM-DD HH:MM:SS");
    if (new Date(history.last_update) < new Date(path_date)) {
      break;
    }
    split_index = i;
  }

  aggregated_path = history;
  let diff = path.length - split_index;
  if (diff < 2) return [];
  return path;
};

export const fetchVehicleDetail = (
  api_key,
  vehicle_id,
  start_date,
  end_date
) => (dispatch) => {
  dispatch(fetchVehicleStatRequest(start_date));
  fetchVehicleDataApi(api_key, vehicle_id, start_date, end_date)
    .then((data) => {
      if (data.response.length <= 1) {
        dispatch(
          fetchVehicleStatFailure(
            "No trips found for this vehicle at the selected date."
          )
        );
      } else {
        FetchVehicleHistory(vehicle_id, start_date).then((result) => {
          if (result.length > 0) id = result.id;
          object_id = vehicle_id;
          query_date = start_date;
          //let path = filterData(data.response, result)
          paths = data.response;
          fetchPath(dispatch);
        });
      }
    })
    .catch((error) => {
      let message =
        error.response !== undefined
          ? error.response.data.errormessage
          : "Technical error occurred. Contact developer";
      dispatch(fetchVehicleStatFailure(message));
    });
};
