import React, { useState } from "react";
import {Container, Table} from 'semantic-ui-react';
import { cleanData } from "../../actions/VehiclesAction"
import './JsonTable.scss'

const JsonTable = (props) => {
    const [index, setIndex] = useState({current: null})
    const {columns, rows, tableAction} = props

    return (
        <Container className={"mb-30"}>
            <Table celled className={"action-table"}>
                <Table.Header>
                    <Table.Row>
                        {columns.map((value, key) => {
                            return <Table.HeaderCell key={key}>{value.name}</Table.HeaderCell>
                        })}
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {rows.map((row, key) => {
                        row = cleanData(row)
                        return <Table.Row key={key} onClick={() => { setIndex(key); tableAction(row.objectId, row.timestamp)}} className={ index === key ? "active-row" : "default-row"}>
                                {columns.map((col, index) => {
                                    return <Table.Cell key={index}>{row[col.key]}</Table.Cell>
                                })}
                        </Table.Row>
                    })}
                </Table.Body>
            </Table>
        </Container>
    )
}

export default JsonTable;