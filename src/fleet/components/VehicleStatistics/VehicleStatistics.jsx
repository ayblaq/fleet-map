import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { DateSelectorComponent, InfoTable } from "../../components/Index";
import { MessageBox, ProgressLoader } from "../../../common/components/index";
import { Container } from "semantic-ui-react";
import { fetchVehicleDetail } from "../../actions/VehicleStatisticAction";
import moment from "moment";

const VehiclesStatistics = (props) => {
  const {
    vehicle_details: {
      data,
      vehicleId,
      isFetching,
      error,
      errorMessage,
      maxDate,
      vehiclePath,
      optimisedPath,
    },
  } = useSelector((state) => state.fleet);

  const { api_key } = props;

  const [selected_date, setValue] = useState({ value: "" });

  const dispatch = useDispatch();

  const handleChange = (date) => {
    setValue({ value: date });
  };

  const getData = () => {
    if (selected_date.value && vehicleId) {
      let start_date = moment(selected_date.value, "dd/MM/yyyy");
      let end_date = moment(selected_date.value, "dd/MM/yyyy").add(1, "d");
      dispatch(
        fetchVehicleDetail(
          api_key,
          vehicleId,
          start_date.format("YYYY-MM-DD"),
          end_date.format("YYYY-MM-DD")
        )
      );
    }
  };

  const renderData = () => {
    if (data.length > 0) {
      let result = aggregateData();
      return <InfoTable rows={result}></InfoTable>;
    } else if (isFetching) {
      return (
        <ProgressLoader
          size={"small"}
          description={"Fetching data"}
        ></ProgressLoader>
      );
    } else if (error) {
      return <MessageBox type={"ban"} details={errorMessage}></MessageBox>;
    }
  };

  const computeDistance = (path) => {
    let distance = 0;
    if (Object.keys(path).length > 0) {
      path.routes[0].legs.map((value, index) => {
        return (distance += value.distance.value);
      }, distance);
    }

    return distance;
  };

  const distanceUnit = (distance) => {
    if (distance > 1000) {
      distance = distance * 0.001;
      distance = `${distance.toFixed(2)} Km`;
    } else {
      distance = `${distance.toFixed(2)} m`;
    }

    return distance;
  };

  const calcStop = () => {
    let stops = 0;
    for (let i = 0; i < data.length - 1; i++) {
      let item = data[i];
      if (item.Latitude === null || item.Longitude === null) continue;
      if (
        data[i + 1]["Latitude"] !== item.Latitude &&
        data[i + 1]["Longitude"] !== item.Longitude &&
        item.Speed === null && item.EngineStatus === null
      ) {
        stops += 1;
      }
    }

    return stops;
  };
  
  const pathDistance = (path) => {
    let distance = 0;
    path.map((row, index) => {
      return (distance += computeDistance(row));
    }, distance);
    
    return distanceUnit(distance);
  }

  const aggregateData = () => {
    let optDistance = pathDistance(optimisedPath);
    let vehicleDistance = pathDistance(vehiclePath)

    return [
      { name: "Total Distance", value: vehicleDistance },
      { name: "Shortest possible distance", value: optDistance },
      { name: "Number of stops", value: calcStop() },
    ];
  };

  return (
    <Container>
      <DateSelectorComponent
        searchAction={getData}
        selected_date={selected_date}
        handleChange={handleChange}
        maxDate={maxDate}
      ></DateSelectorComponent>
      {renderData()}
    </Container>
  );
};

export default VehiclesStatistics;
