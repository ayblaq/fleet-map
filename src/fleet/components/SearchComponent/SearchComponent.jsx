import React from "react";
import { Container, Form, Input } from "semantic-ui-react";
import "./SearchComponent.scss";

const SearchComponent = (props) => {
  const { searchAction, search, handleChange, placeHolder, name, labelTitle } = props;

  return (
    <Container>
      <Form onSubmit={searchAction}>
        <Form.Group>
          <Form.Field inline>
          <label>{labelTitle}</label>
          <Input
            placeholder={placeHolder}
            name={name}
            value={search.key}
            onChange={handleChange}
          />
          </Form.Field>
          <Form.Field>
            <Form.Button content="Go" className={"go-button"} />
          </Form.Field>
        </Form.Group>
      </Form>
    </Container>
  );
};

export default SearchComponent;
