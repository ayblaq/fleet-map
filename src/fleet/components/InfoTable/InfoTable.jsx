import React from 'react';
import {Container, Table} from 'semantic-ui-react';
import './InfoTable.scss'


const InfoTable = (props) => {
    const { rows } = props

    return (
        <Container>
            <Table celled>
                <Table.Header>
                </Table.Header>
                <Table.Body>
                    {rows.map((row, key) => {
                        return <Table.Row key={key}>
                            {Object.keys(row).map((value, index) => {
                                return <Table.Cell key={index}>{row[value]}</Table.Cell>
                            })}
                        </Table.Row>
                    })}
                </Table.Body>
            </Table>
        </Container>
    )
}

export default InfoTable;