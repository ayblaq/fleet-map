import React, { useRef, useEffect } from "react";
import { useSelector } from "react-redux";
import { withScriptjs, withGoogleMap, GoogleMap, Marker, DirectionsRenderer } from "react-google-maps";
import marker_pin  from '../../images/map-pin-solid.svg'

const MapComponent = withScriptjs(
    withGoogleMap(
      ({ locations }) => {
        const mapRef = useRef(null);

        const {
            vehicle_details: { optimisedPath, vehiclePath, vehicleId },
          } = useSelector((state) => state.fleet);

        const fitBounds = () => {
          const bounds = new window.google.maps.LatLngBounds();
          locations.map(item => {
              if (item.objectId === vehicleId || vehicleId === null) {
                bounds.extend({lat: item.latitude, lng: item.longitude});
              }
              return item.id
          });
          mapRef.current.fitBounds(bounds);
        };
    
        useEffect(() => {
          fitBounds();
        });

        const defaultMapOptions = {
            fullscreenControl: false,
            disableDefaultUI: true,
            styles: [
                {
                    featureType: 'poi',
                    stylers: [{visibility: 'off'}]
                },
                {
                    featureType: 'transit',
                    elementType: 'labels.icon',
                    stylers: [{visibility: 'off'}]
                }
            ]
        };

        const labelOptions = {
            fontFamily: "Arial",
            color: "black",
            fontSize: "12px"
        }

        const image = {
            url: marker_pin,
            labelOrigin: new window.google.maps.Point(20, -5),
            scaledSize: new window.google.maps.Size(25, 25)
        }

        const vehicleLine = {
            polylineOptions: {
                strokeColor: '#FFFF00',
                strokeOpacity: 0.2,
                strokeWeight: 6
            },
            suppressMarkers: true,
        };

        const optimisedLine = {
            polylineOptions: {
                strokeColor: '#00000',
                strokeOpacity: 1,
                strokeWeight: 3
            },
            suppressMarkers: true,
        };

        const renderMarker = () => {
            return locations.map((item, index) => {
                if (item.objectId === vehicleId || vehicleId === null) {
                    let position = {lat: parseFloat(item.latitude), lng: parseFloat(item.longitude)}
                    return <Marker key={index} position={position} icon={image} label={{text: vehicleId === null ? " " : item.objectName, ...labelOptions}} />
                }
            })
        }

        const renderPath = (route, option, rIndex) => {
            return route.map((row, index) => {
                return <DirectionsRenderer directions={row} routeIndex={rIndex} key={index} options={option}/>
            })
        }
  
        return (
          <GoogleMap ref={mapRef} mapTypeId={'roadmap'} defaultOptions={defaultMapOptions}>
            {renderMarker()}
            { renderPath(vehiclePath, vehicleLine, 2) }
            { renderPath(optimisedPath, optimisedLine, 1) }
          </GoogleMap>
        );
      })
  );

  export default MapComponent