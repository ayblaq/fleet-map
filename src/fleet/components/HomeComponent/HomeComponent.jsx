import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { SearchComponent, JsonTable, VehicleStatistics, MapComponent } from "../../components/Index";
import { MessageBox, ProgressLoader } from "../../../common/components/index";
import { Grid, Container } from "semantic-ui-react";
import { fetchVehicles } from "../../actions/VehiclesAction"
import { setVehicleStatId } from "../../actions/VehicleStatisticAction"
import { config } from '../../../config';

const HomeComponent = () => {
  const {
    vehicles: { vehicles, isFetching, error, errorMessage },
  } = useSelector((state) => state.fleet);

  const dispatch = useDispatch();

  const [search, setValue] = useState({ key: "" });

  const handleChange = (e) => {
    setValue({ key: e.target.value });
  };

  const getData = () => {
    if (search.key) {
      dispatch(fetchVehicles(search.key))
    }
  };

  const renderData = () => {
    if (vehicles.length === 0 && !isFetching) {
      let box_type = "info";
      let box_detail = "Please enter API key to fetch data";
      if (error) {
        box_type = "ban";
        box_detail = errorMessage;
      }
      return <MessageBox type={box_type} details={box_detail}></MessageBox>;
    } else if (isFetching) {
      return (
        <ProgressLoader
          size={"small"}
          description={"Fetching data"}
        ></ProgressLoader>
      );
    } else if (vehicles.length > 0) {
      return renderTable();
    }
  };

  const rowClick = (objectId, timestamp) => {
    dispatch(setVehicleStatId(objectId, new Date(timestamp)))
  }

  const renderTable = () => {
    let columns = [{name: 'Name', key: 'objectName'}, {name: 'Speed', key: 'speed'}, {name: 'Last Update', key: 'time_ago'}]
    return <Grid columns={2} stackable>
      <Grid.Column width={7}>
        <JsonTable columns={columns} rows={vehicles} tableAction={rowClick}></JsonTable>
          <VehicleStatistics api_key={search.key}></VehicleStatistics>
          </Grid.Column>
          <Grid.Column width={9}>
          {renderMap()}
        </Grid.Column>  
      </Grid>
  };

  const renderMap = () => {
    return <MapComponent 
              locations={vehicles}
              disableDefaultUI={true}
              googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${config.mapKey}&libraries=geometry,drawing,places`}
              loadingElement={<ProgressLoader size={"small"} description={"Drawing Map"}></ProgressLoader>}
              containerElement={<div className="mapContainer" style={{ height: "400px" }} />}
              mapElement={<div className="map" style={{ height: '100%' }} />}
              ></MapComponent>
  }

  return (
    <Container className={"mt-10"}>
      <SearchComponent
        searchAction={getData}
        search={search}
        placeHolder={"(api key goes here)"}
        name={"search"}
        labelTitle={"API key:"}
        handleChange={handleChange}
      ></SearchComponent>
      {renderData()}
    </Container>
  );
};

export default HomeComponent;
