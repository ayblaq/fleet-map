import React from "react";
import { Container, Form } from "semantic-ui-react";
import DatePicker from "react-datepicker";
import "./DateSelectorComponent.scss";
import "react-datepicker/dist/react-datepicker.css";

const DateSelectorComponent = (props) => {
  const { searchAction, selected_date, handleChange, maxDate } = props;

  return (
    <Container>
      <Form onSubmit={searchAction}>
        <Form.Group inline>
            <Form.Field inline>
                <label>Date:</label>
                <DatePicker
                  selected={selected_date.value}
                  maxDate={maxDate}
                  onChange={(date) => handleChange(date)}
                  placeholderText="(desired date)"
                  dateFormat="dd/MM/yyyy"
              />
            </Form.Field>
            <Form.Field inline>
                <Form.Button content="Go" className={"go-button"} />
            </Form.Field>
        </Form.Group>
      </Form>
    </Container>
  );
};

export default DateSelectorComponent;
