import { useIndexedDB } from 'react-indexed-db';

export const FetchVehicleHistory = async (vehicle_id, query_date) => {
    const { getAll } = useIndexedDB('vehicle_path_history');
    let history_data = []
    return await getAll().then((data) => {
        if (data !== undefined) {
            for (let i in data) {
                let row = data[i]
                if (row.query_date === query_date && row.vehicle_id === vehicle_id) {
                    history_data.push(JSON.parse(row.data))
                }
            }
        }
        return history_data
    }).catch((error) => {
        return []
    })
}

export const AddVehicleHistory = (id, vehicle_id, query_date, data, last_update) => {
    const { update, add } = useIndexedDB('vehicle_path_history');
    if (id === null) {
        add({
            vehicle_id: vehicle_id,
            query_date: query_date,
            data: JSON.stringify(data),
            last_update: last_update 
         })
    } else {
        update({
            id: id,
            vehicle_id: vehicle_id,
            query_date: query_date,
            data: data,
            last_update: last_update 
         })
    }
}