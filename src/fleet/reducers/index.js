import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { vehiclesReducer } from "./VehiclesReducer";
import { vehicleStatisticReducer } from "./VehicleStatisticReducer";

const persistConfig = {
  key: "fleet",
  storage: storage,
};

const fleetReducers = (state = {}, action) => {
    return {
        vehicles: vehiclesReducer(state.vehicles, action),
        vehicle_details: vehicleStatisticReducer(state.vehicle_details, action)
    }
}

const reducer = persistReducer(persistConfig, fleetReducers)

export { reducer as fleetReducers }