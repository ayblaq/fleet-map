import {
    FETCH_VEHICLE_STATS_FAILURE,
    FETCH_VEHICLE_STATS_REQUEST,
    FETCH_VEHICLE_STATS_SUCCESS,
    SET_VEHICLE_ID
} from '../actions/VehicleStatisticAction'

export const vehicleStatisticReducer = (state = {
    isFetching: false,
    vehicleId: null,
    isReset: false,
    data: [],
    errorMessage: '',
    error: false,
    optimisedPath: [],
    vehiclePath: [],
    current_start_date: null,
    maxDate: new Date()
}, action) => {
    switch (action.type) {
        case FETCH_VEHICLE_STATS_REQUEST:
            return {
                ...state,
                isFetching: true,
                error: false,
                isReset: false,
                current_start_date: action.payload.start_date,
                data: [],
                optimisedPath: [],
                vehiclePath: []
            };
        case FETCH_VEHICLE_STATS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                data: action.payload.data,
                optimisedPath: action.payload.optimised_path,
                vehiclePath: action.payload.vehicle_path,
                error: false,
                isReset: false
            }
        case FETCH_VEHICLE_STATS_FAILURE:
            return {
                ...state,
                isFetching: false,
                errorMessage: action.payload.message,
                error: true,
                data: [],
                optimisedPath: [],
                vehiclePath: [],
                isReset: false
            } 
        case SET_VEHICLE_ID:
            return {
                ...state,
                isReset: true,
                vehicleId: action.payload.vehicle_id,
                maxDate: action.payload.max_date,
                optimisedPath: [],
                vehiclePath: [],
                data: [],
            }
        default:
            return state     
    }
}