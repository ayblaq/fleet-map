import {
    FETCH_VEHICLES_FAILURE,
    FETCH_VEHICLES_REQUEST,
    FETCH_VEHICLES_SUCCESS
} from '../actions/VehiclesAction'

export const vehiclesReducer = (state = {
    isFetching: false,
    vehicles: [],
    errorMessage: '',
    error: false
}, action) => {
    switch (action.type) {
        case FETCH_VEHICLES_REQUEST:
            return {
                ...state,
                isFetching: true,
                error: false
            };
        case FETCH_VEHICLES_SUCCESS:
            return {
                ...state,
                isFetching: false,
                vehicles: action.payload.vehicles,
                error: false
            }
        case FETCH_VEHICLES_FAILURE:
            return {
                ...state,
                isFetching: false,
                errorMessage: action.payload.message,
                error: true,
                vehicles: []
            } 
        default:
            return state     
    }
}