import React from "react";
import { Container } from "semantic-ui-react";
import "./HomeContainer.scss";
import { SiteHeader } from "../../common/components";
import {HomeComponent} from "../../fleet/components/Index";

const HomeContainer = () => {

  return (
    <div>
      <SiteHeader />
      <Container className={"view-container"}>
        <HomeComponent></HomeComponent>
      </Container>
      {/* <Footer /> */}
    </div>
  );
  
};

export default HomeContainer;
