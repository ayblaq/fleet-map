import React from 'react'
import { Route, Redirect, Switch } from 'react-router'
import { createBrowserHistory } from 'history'
import { HomeContainer } from "../../src/containers"
import App from "../app/components/App/App";


export const history = getHistory();

export const appRouting = [
    {
        path: '/',
        name: 'Home',
        exact: true,
        tag: Route,
        component: HomeContainer
    },
];

export const Routing = () => {
    let routes = appRouting.filter(a => a.tag || a.component);
    let routesRendered = routes.map((a, i) => {
        let Tag = a.tag;
        let {path, exact, strict, component} = a;
        let b = {path, exact, strict, component};
        return <Tag key={i} {...b} />
    });

    return (
        <App>
            <Switch>
                {routesRendered}
                <Redirect to="/"/>
            </Switch>
        </App>
    )
};

function getHistory() {
    const basename = process.env.BUILD_DEMO ? '/react-semantic.ui-starter' : '';
    return createBrowserHistory({basename})
}
