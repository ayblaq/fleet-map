export const DBConfig = {
    name: 'FleetHistory',
    version: 1,
    objectStoresMeta: [
      {
        store: 'vehicle_path_history',
        storeConfig: { keyPath: 'id', autoIncrement: true },
        storeSchema: [
          { name: 'vehicle_id', keypath: 'vehicle_id', options: { unique: false } },
          { name: 'last_update', keypath: 'last_update', options: { unique: false } },
          { name: 'query_date', keypath: 'query_date', options: { unique: false } },
          { name: 'data', keypath: 'data', options: { unique: false } }
        ]
      }
    ]
  };