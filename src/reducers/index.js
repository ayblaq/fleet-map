import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import { fleetReducers } from '../fleet/reducers'

export const createRootReducer = (history) => combineReducers({
  router: connectRouter(history),
  fleet: fleetReducers
});